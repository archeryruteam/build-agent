# About
Docker image to build and deploy Hugo site with help of `rclone` utility.

## How to build image
Localy:
```
docker build -t hugo_builder .
```

Also, you may specify this repo as source for automatically build on https://hub.docker.com.

## How to use
1. Put `bitbucket-pipelines.yml` into root of the project
2. Specify docker image 
3. Enable Bitbucket-pipelines for a project
4. Specify ENV variables into Pipelines settings. For example:
```
RCLONE_SELECTEL_TYPE = swift
RCLONE_SELECTEL_USER = LOGIN
RCLONE_SELECTEL_KEY = PASSWORD
RCLONE_SELECTEL_AUTH = https://auth.selcdn.ru/v1.0
```
